pragma solidity ^0.4.24;
import "./Owned.sol";
import "./InvestmentPlatformI.sol";
import "./AssetTokens.sol";

contract InvestmentPlatform is Owned {
    
    mapping(address => bool) approvedInvestors;
    event LogCreateAsset(address indexed owner, bytes32 name, uint totalSupplyTokens, uint priceToken);
    event LogSetStatus(address indexed investor, bool status);
    event LogAuthorizeAssetSale(address indexed asset, address indexed buyer, uint price);
    event LogDeleteAsset(address indexed assetAdress);
    
    function createNewAsset(address assetOwner, bytes32 name, uint totalSupplyTokens, uint priceToken) 
    fromOwner public returns(AssetTokens _newAssetTokens) {
        require(approvedInvestors[assetOwner], "Asset owner is not an approved investor");
        AssetTokens newAssetTokens = new AssetTokens(name, totalSupplyTokens, priceToken, address(this));
        newAssetTokens.setOwner(assetOwner);
        emit LogCreateAsset(assetOwner, name, totalSupplyTokens, priceToken); 
        return newAssetTokens;
    }
    
    function setInvestorStatus(address investor, bool status) public fromOwner returns(bool success) {
        require(approvedInvestors[investor] != status, "status already set to this value");
        approvedInvestors[investor] = status ;
        emit LogSetStatus(investor, status);
        return true;
    }
    
    function getInvestorStatus(address investor) public view returns (bool status) {
        return approvedInvestors[investor];
    }
    
    function authorizeAssetSale(address assetTokens, address buyer, uint price) public fromOwner returns (bool status) {
        require(approvedInvestors[buyer] != status, "Asset buyer is not authorized");
        emit LogAuthorizeAssetSale(assetTokens, buyer, price);
        return AssetTokens(assetTokens).approveAssetSale(buyer, price);
    }
    
    function deleteAsset (address assetAdress) public fromOwner returns (bool success)  {
        AssetTokens asset = AssetTokens(assetAdress);
        asset.killAsset();
        emit LogDeleteAsset(assetAdress);
        return true;
    }
}